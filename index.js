function printInput(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

//printInput();

// name is a parameter
function printName(name){
	console.log("My name is " + name);
}

// ("John") is an argument
//printName("John");

// variables passed as an argument
let sampleVariable = "James";
printName(sampleVariable);

// Mini activity
// Create two different functions that will display a person's age and location using function parameters and arguments.

// function for displaying person's age
function personAge(age){
	console.log("Age is " + age);
}

personAge(25);

// function for displaying person's location
function personLocation(location){
	console.log("You're located in " + location);
}

personLocation("Manila");

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
// Displaying more information about the argumentFunction
console.log(argumentFunction);

// Using multiple parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('John', 'Peters', 'Smith');
createFullName('Jane', 'North');
createFullName('James', 'Barnes', 'Rogers', 'Stark');

function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

printFullName('John', 'Peters', 'Smith');

// Return statement
// allows to output value from a function to be passed to the line/block of code that invoked the function.

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This message will not be printed");
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

function returnAddress(city, country){
	let fullAddress = city + " , " + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu city", "Cebu");
console.log(myAddress);

function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);